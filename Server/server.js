//import express from 'express';
//import cors from 'cors';
//import bodyParser from 'body-parser';
//import mongojs from 'mongojs';

const cors = require('cors');
const bodyParser = require('body-parser')
const mongojs = require('mongojs')
const express = require("express");

const app = express();
const router = express.Router();
const db = mongojs('KGD6-AR2-DEV1-ST1', ['Log']);

app.use(cors());
app.use(bodyParser.json());

var d = new Date();
var curr_date = (d.getDate());
var curr_month = d.getMonth();
curr_month++;
var curr_year = d.getFullYear();
var TodayDate = curr_year + "-" + curr_month + "-" + curr_date;

router.route('/insights/:data').get((req, res) => {
    let TodayDate = req.params.data;
    
    // Get Actual data
    db.Log.find({"DATETIME" : {$regex : TodayDate}}, function (err, docs) {
        if(err) {
            console.log(err);
        }
        else{
            if(docs !== null)
            {
                var objJSON = JSON.stringify(docs);
                res.send(objJSON);
            }
        }
    });
});

router.route('/insights/TorqueData').post((req, res) => {
    
    db.Torque.remove({ date: TodayDate }, function (err, doc) {
		res.json(doc);
    });

    db.Torque.save({ date: TodayDate, data: req.body });
});

router.route('/insights/SPPData').post((req, res) => {

    db.SPP.remove({ date: TodayDate }, function (err, doc) {
		res.json(doc);
    });

    db.SPP.save({ date: TodayDate, data: req.body });
});

router.route('/insights/ECDData').post((req, res) => {

    db.ECD.remove({ date: TodayDate }, function (err, doc) {
		res.json(doc);
    });
    
    db.ECD.save({ date: TodayDate, data: req.body });
});

router.route('/insights/getTorqueData/:data').get((req, res) => {
    let TodayDate = req.params.data;
    
    // Get Torque Data
    db.Torque.findOne({"date" : {$regex : TodayDate}}, function (err, docs) {
        if(err) {
            console.log(err);
        }
        else{
            if(docs !== null)
            {
                var objJSON = JSON.stringify(docs);
                res.send(objJSON);
            }
        }
    });
});

router.route('/insights/getSPPData/:data').get((req, res) => {
    let TodayDate = req.params.data;
    
    // Get Torque Data
    db.SPP.findOne({"date" : {$regex : TodayDate}}, function (err, docs) {
        if(err) {
            console.log(err);
        }
        else{
            if(docs !== null)
            {
                var objJSON = JSON.stringify(docs);
                res.send(objJSON);
            }
        }
    });
});

router.route('/insights/getECDData/:data').get((req, res) => {
    let TodayDate = req.params.data;
    
    // Get Torque Data
    db.ECD.findOne({"date" : {$regex : TodayDate}}, function (err, docs) {
        if(err) {
            console.log(err);
        }
        else{
            if(docs !== null)
            {
                var objJSON = JSON.stringify(docs);
                res.send(objJSON);
            }
        }
    });
});

app.use('/', router);
app.listen(4002, () => console.log('Express server running on port 4002'));