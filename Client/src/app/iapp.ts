export interface iApp {
    wellname: String;
    datetime: String;
    md: String;
    tvd: String;
    spp: String;
    hookload: String;
    rop: String;
    torque: String;
    rpm: String;
    ff: String;
    wob: String;
    flow: String;
    gpm: String;
}