import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class AppService {

  constructor(private http: HttpClient) { }

  getDepthData(TodayDate) {
      return this.http.get('http://localhost:4001/insights/' + TodayDate);
  }

  createTorqueData(arrTorqueDataPoints)
  {
    return this.http.post('http://localhost:4001/insights/TorqueData', arrTorqueDataPoints);
  }

  createSPPData(arrSPPDataPoints)
  {
    return this.http.post('http://localhost:4001/insights/SPPData', arrSPPDataPoints);
  }

  createECDData(arrECDDataPoints)
  {
    return this.http.post('http://localhost:4001/insights/ECDData', arrECDDataPoints);
  }

  getTorqueData(TodayDate) {
    return this.http.get('http://localhost:4001/insights/getTorqueData/' + TodayDate);
  }

  getSPPData(TodayDate) {
    return this.http.get('http://localhost:4001/insights/getSPPData/' + TodayDate);
  }

  getECDData(TodayDate) {
    return this.http.get('http://localhost:4001/insights/getECDData/' + TodayDate);
  }

}