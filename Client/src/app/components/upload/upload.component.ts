import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from '../../app.service';

import * as XLSX from 'xlsx';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})

export class UploadComponent implements OnInit {
	arrayBuffer:any;
	TorqueFile:File;
	SPPFile:File;
	ECDFile:File;
	arrTorqueDataPoints = [];
	arrSPPDataPoints = [];
	arrECDDataPoints = [];

	TorqueColumns = ["FF", "Depth", "Torque"];
	SPPColumns = ["SPPPlot", "Depth", "SPP"];
	ECDColumns = ["ECDPlot", "Depth", "ECD"];

	constructor(private appservice: AppService, private router: Router, private route: ActivatedRoute, private datePipe: DatePipe) { }
	
  	ngOnInit() {
	}

	// Set the Data
	onchangeTorqueData(event) 
  	{
		this.TorqueFile = event.target.files[0];
	}

	onchangeSPPData(event) 
  	{
		this.SPPFile = event.target.files[0];
	}

	onchangeECDData(event) 
  	{
		this.ECDFile = event.target.files[0];
	}
	
	// Load & display the data
	onuploadTorqueData() {
		let fileReader = new FileReader();
			fileReader.onload = (e) => {
					this.arrayBuffer = fileReader.result;
					var data = new Uint8Array(this.arrayBuffer);
					var arr = new Array();
					for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
					var bstr = arr.join("");
					var workbook = XLSX.read(bstr, {type:"binary"});
					var first_sheet_name = workbook.SheetNames[0];
					var worksheet = workbook.Sheets[first_sheet_name];
					this.arrTorqueDataPoints = (XLSX.utils.sheet_to_json(worksheet,{raw:true}));
			}
			fileReader.readAsArrayBuffer(this.TorqueFile);
	}

	onuploadSPPData() {
		let fileReader = new FileReader();
			fileReader.onload = (e) => {
					this.arrayBuffer = fileReader.result;
					var data = new Uint8Array(this.arrayBuffer);
					var arr = new Array();
					for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
					var bstr = arr.join("");
					var workbook = XLSX.read(bstr, {type:"binary"});
					var first_sheet_name = workbook.SheetNames[0];
					var worksheet = workbook.Sheets[first_sheet_name];
					this.arrSPPDataPoints = (XLSX.utils.sheet_to_json(worksheet,{raw:true}));
			}
			fileReader.readAsArrayBuffer(this.SPPFile);
	}

	onuploadECDData() {
		let fileReader = new FileReader();
			fileReader.onload = (e) => {
					this.arrayBuffer = fileReader.result;
					var data = new Uint8Array(this.arrayBuffer);
					var arr = new Array();
					for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
					var bstr = arr.join("");
					var workbook = XLSX.read(bstr, {type:"binary"});
					var first_sheet_name = workbook.SheetNames[0];
					var worksheet = workbook.Sheets[first_sheet_name];
					this.arrECDDataPoints = (XLSX.utils.sheet_to_json(worksheet,{raw:true}));
			}
			fileReader.readAsArrayBuffer(this.ECDFile);
	}

	// Save the data
	SaveTorque()
	{
		this.appservice.createTorqueData(this.arrTorqueDataPoints).subscribe(res=>{
			this.reload();
		});
	}

	SaveSPP()
	{
		this.appservice.createSPPData(this.arrSPPDataPoints).subscribe(res=>{
			this.reload();
		});
	}

	SaveECD()
	{
		this.appservice.createECDData(this.arrECDDataPoints).subscribe(res=>{
			this.reload();
		});
	}

	reload()
	{
		this.ngOnInit();
	}
}
