import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from '../../app.service';
import { iApp } from '../../iapp';

import * as CanvasJS from '../../Jquery/CanvasJS.min';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  date: Date;
  TorqueActualData: iApp[];
  TorquePlaningData: iApp[];
  constructor(private appservice: AppService, private router: Router, private route: ActivatedRoute, private datePipe: DatePipe) { }

  ngOnInit() {
    this.GenerateTorquePlot(), this.GenerateSPPPlot(), this.GenerateECDPlot();
  }

  GenerateTorquePlot()
  {
    // Local Variable
    let arrActualDataPoints = [];
    let arrActualDataPointsWOB = [];
    let arrActualDataPointsRBM = [];
    let arrPlannedDataPointsFF1 = [];
    let arrPlannedDataPointsFF2 = [];
    let arrPlannedDataPointsFF3 = [];

    let base;
    let depth : number;
    let torque : number;
    let WOB : number;
    let RBM : number;
    let FF : number;

    this.date =  new Date();
    let todaydate = this.datePipe.transform(this.date,"yyyy-MM-dd");
    
    // Plot the actual graphs
    this.appservice.getDepthData(todaydate).subscribe((ActualRes: iApp[]) => {
      // Check the response is null
      if(ActualRes.length !== 0)
      {
        for (var i = 0; i < ActualRes.length; i++)
        {
          base = ActualRes[i];          
          depth = base["MD"];
          torque = base["TORQUE"];
          WOB = base["WOB"];
          RBM = base["RBM"];

          // Convert to number
          depth = +depth;
          torque = +torque;
          WOB = +WOB;
          RBM = +RBM;
    
          // Add the number to array
          arrActualDataPoints.push({ x: torque, y: depth });
          arrActualDataPointsWOB.push({ x: WOB, y: depth });
          arrActualDataPointsRBM.push({ x: RBM, y: depth });
        }
      }

      // Plot the planing graphs
      this.appservice.getTorqueData(todaydate).subscribe((PlaningRes: iApp[]) => {
        // Check the response is null        
        if(PlaningRes.length !== 0)
        {
          let TorquePlanData = PlaningRes["data"];
          for (var i = 0; i < TorquePlanData.length; i++)
          {
            base = TorquePlanData[i];
            FF = base["FF"];
            depth = base["Depth"];
            torque = base["Torque"];

            // Convert to number
            FF = +FF;
            depth = +depth;
            torque = +torque;
      
            // Add the number to array
            if(FF == 0.2)
            {
              arrPlannedDataPointsFF1.push({ x: torque, y: depth });
            }
            if(FF == 0.25)
            {
              arrPlannedDataPointsFF2.push({ x: torque, y: depth });
            }
            if(FF == 0.3)
            {
              arrPlannedDataPointsFF3.push({ x: torque, y: depth });
            }
          }
        }

        // Display the Chart
        let chart = new CanvasJS.Chart("TorqueContainer", {
          animationEnabled: true,
          exportEnabled: true,
          theme: "light2",
          axisY: {
              title: "Depth (in meters)",
              suffix: " m",
              reversed: true,
              minimum: 3000,
              maximum: 5000
          },
          axisX2: [
          {
              tickThickness: 0,
              labelAngle: 0,
              minimum: 0,
              maximum: 50
          }],
          axisX: [
          {
              tickThickness: 0,
              labelAngle: 0,
              minimum: 0,
              maximum: 50
          }],
          toolTip: {
              shared: true
          },
          legend: {
              cursor: "pointer",
              itemclick: toggleDataSeries
          },
          data: [{
            type: "scatter",
            name: "Actual",
            color: "#333333",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            markerType: "circle",
            markerSize: 4,
            dataPoints: arrActualDataPoints
          },
          {
            type: "line",
            name: "WOB",
            color: "#0000FF",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            dataPoints: arrActualDataPointsWOB
          },
          {
              type: "line",
              name: "RPM",
              color: "#7F6084",
              showInLegend: true,
              axisXType: "secondary",
              yValueFormatString: "#,##0 meters",
              dataPoints: arrActualDataPointsRBM
          },
          {
            type: "line",
            name: "FF: 0.20",
            color: "#C24642",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            dataPoints: arrPlannedDataPointsFF1
          },
          {
            type: "line",
            name: "FF: 0.25",
            color: "#369EAD",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            dataPoints: arrPlannedDataPointsFF2
          },
          {
            type: "line",
            name: "FF: 0.30",
            color: "#FF69B4",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            dataPoints: arrPlannedDataPointsFF3
          }]
        });
        chart.render();
      });
    });
  }

  GenerateSPPPlot()
  {
    // Local Variable
    let arrActualDataPoints = [];
    let arrActualDataPointsFlowRate = [];
    let arrPlannedDataPointsSPP1 = [];
    let arrPlannedDataPointsSPP2 = [];
    let arrPlannedDataPointsSPP3 = [];

    let base;
    let depth : number;
    let SPP : number;
    let FlowRate : number;
    let SPPPlot : number;

    this.date =  new Date();
    let todaydate = this.datePipe.transform(this.date,"yyyy-MM-dd");

    // Plot the actual graphs
    this.appservice.getDepthData(todaydate).subscribe((ActualRes: iApp[]) => {
      // Check the response is null
      if(ActualRes.length !== 0)
      {
        for (var i = 0; i < ActualRes.length; i++)
        {
          base = ActualRes[i];          
          depth = base["MD"];
          SPP = base["SPP"];
          FlowRate = 0;

          // Convert to number
          depth = +depth;
          SPP = +SPP;
          FlowRate = +FlowRate;
    
          // Add the number to array
          arrActualDataPoints.push({ x: SPP, y: depth });
          arrActualDataPointsFlowRate.push({ x: FlowRate, y: depth });
        }
      }

      // Plot the planing graphs
      this.appservice.getSPPData(todaydate).subscribe((PlaningRes: iApp[]) => {
        // Check the response is null        
        if(PlaningRes.length !== 0)
        {
          let SPPPlanData = PlaningRes["data"];
          for (var i = 0; i < SPPPlanData.length; i++)
          {
            base = SPPPlanData[i];
            SPPPlot = base["SPPPlot"];
            depth = base["Depth"];
            SPP = base["SPP"];
            
            // Convert to number
            SPPPlot = +SPPPlot;
            depth = +depth;
            SPP = +SPP;
      
            // Add the number to array
            if(SPPPlot == 900)
            {
              arrPlannedDataPointsSPP1.push({ x: SPP, y: depth });
            }
            if(SPPPlot == 950)
            {
              arrPlannedDataPointsSPP2.push({ x: SPP, y: depth });
            }
            if(SPPPlot == 1000)
            {
              arrPlannedDataPointsSPP3.push({ x: SPP, y: depth });
            }
          }
        }

        // Display the Chart
        let chart = new CanvasJS.Chart("SPPContainer", {
          animationEnabled: true,
          exportEnabled: true,
          theme: "light2",
          axisY: {
              title: "Depth (in meters)",
              suffix: " m",
              reversed: true,
              includeZero: false
          },
          axisX2: [
          {
              tickThickness: 0,
              labelAngle: 0,
              includeZero: false
          }],
          axisX: [
          {
              tickThickness: 0,
              labelAngle: 0,
              includeZero: false
          }],
          toolTip: {
              shared: true
          },
          legend: {
              cursor: "pointer",
              itemclick: toggleDataSeries
          },
          data: [{
            type: "scatter",
            name: "Actual",
            color: "#333333",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            markerType: "circle",
            markerSize: 4,
            dataPoints: arrActualDataPoints
          },
          {
              type: "line",
              name: "RBM",
              color: "#7F6084",
              showInLegend: true,
              axisXType: "secondary",
              yValueFormatString: "#,##0 meters",
              dataPoints: arrActualDataPointsFlowRate
          },
          {
            type: "line",
            name: "FF: 0.20",
            color: "#C24642",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            dataPoints: arrPlannedDataPointsSPP1
          },
          {
            type: "line",
            name: "FF: 0.25",
            color: "#369EAD",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            dataPoints: arrPlannedDataPointsSPP2
          },
          {
            type: "line",
            name: "FF: 0.30",
            color: "#FF69B4",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            dataPoints: arrPlannedDataPointsSPP3
          }]
        });
        chart.render();
      });
    });
  }

  GenerateECDPlot()
  {
    // Variable
    let arrActualDataPoints = [];
    let arrActualDataPointsESD = [];
    let arrActualDataPointsMW = [];
    let arrPlannedDataPointsECD1 = [];
    let arrPlannedDataPointsECD2 = [];
    let arrPlannedDataPointsECD3 = [];

    let base;
    let depth : number;
    let ECD : number;
    let ESD : number;
    let WM : number;
    let ECDPlot : number;

    this.date =  new Date();
    let todaydate = this.datePipe.transform(this.date,"yyyy-MM-dd");

    // Plot the actual graphs
    this.appservice.getDepthData(todaydate).subscribe((ActualRes: iApp[]) => {
      // Check the response is null
      if(ActualRes.length !== 0)
      {
        for (var i = 0; i < ActualRes.length; i++)
        {
          base = ActualRes[i];          
          depth = base["MD"];
          ECD = base["ECD"];
          ESD = base["ESD"];
          WM = base["WM"];

          // Convert to number
          depth = +depth;
          ECD = +ECD;
          ESD = +ESD;
          WM = +WM;
    
          // Add the number to array
          arrActualDataPoints.push({ x: ECD, y: depth });
          arrActualDataPointsESD.push({ x: ESD, y: depth });
          arrActualDataPointsMW.push({ x: WM, y: depth });
        }
      }

      // Plot the planing graphs
      this.appservice.getECDData(todaydate).subscribe((PlaningRes: iApp[]) => {
        // Check the response is null        
        if(PlaningRes.length !== 0)
        {
          let ECDPlanData = PlaningRes["data"];
          for (var i = 0; i < ECDPlanData.length; i++)
          {
            base = ECDPlanData[i];
            ECDPlot = base["ECDPlot"];
            depth = base["Depth"];
            ECD = base["ECD"];

            // Convert to number
            ECDPlot = +ECDPlot;
            depth = +depth;
            ECD = +ECD;
      
            // Add the number to array
            if(ECDPlot == 10)
            {
              arrPlannedDataPointsECD1.push({ x: ECD, y: depth });
            }
            if(ECDPlot == 20)
            {
              arrPlannedDataPointsECD2.push({ x: ECD, y: depth });
            }
            if(ECDPlot == 30)
            {
              arrPlannedDataPointsECD3.push({ x: ECD, y: depth });
            }
          }
        }

        // Display the Chart
        let chart = new CanvasJS.Chart("ECDContainer", {
          animationEnabled: true,
          exportEnabled: true,
          theme: "light2",
          axisY: {
              title: "Depth (in meters)",
              suffix: " m",
              reversed: true,
              includeZero: false
          },
          axisX2: [
          {
              tickThickness: 0,
              labelAngle: 0,
              includeZero: false
          }],
          axisX: [
          {
              tickThickness: 0,
              labelAngle: 0,
              includeZero: false
          }],
          toolTip: {
              shared: true
          },
          legend: {
              cursor: "pointer",
              itemclick: toggleDataSeries
          },
          data: [{
            type: "scatter",
            name: "Actual",
            color: "#333333",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            markerType: "circle",
            markerSize: 4,
            dataPoints: arrActualDataPoints
          },
          {
            type: "line",
            name: "WOB",
            color: "#0000FF",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            dataPoints: arrActualDataPointsESD
          },
          {
              type: "line",
              name: "RBM",
              color: "#7F6084",
              showInLegend: true,
              axisXType: "secondary",
              yValueFormatString: "#,##0 meters",
              dataPoints: arrActualDataPointsMW
          },
          {
            type: "line",
            name: "FF: 0.20",
            color: "#C24642",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            dataPoints: arrPlannedDataPointsECD1
          },
          {
            type: "line",
            name: "FF: 0.25",
            color: "#369EAD",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            dataPoints: arrPlannedDataPointsECD2
          },
          {
            type: "line",
            name: "FF: 0.30",
            color: "#FF69B4",
            showInLegend: true,
            axisXType: "secondary",
            yValueFormatString: "#,##0 meters",
            dataPoints: arrPlannedDataPointsECD3
          }]
        });
        chart.render();
      });
    });
  }
}

function toggleDataSeries(e) {
  if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
      e.dataSeries.visible = false;
  } else {
      e.dataSeries.visible = true;
  }
  e.chart.render();
}
